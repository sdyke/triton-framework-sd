﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triton.Location.Model.Dao {


/// <summary>
/// Filter for IStateDao.
/// </summary>
/// <author>Scott Dyke</author>
/// <created>3/6/14</created>
public class StateFilter
{

	public bool? IsTerritory
	{
		get;
		set;
	}

	public string Code
	{
		get;
		set;
	}

	public int? Id
	{
		get;
		set;
	}
}
}
